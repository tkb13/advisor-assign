#!/usr/bin/python3

import sys,os,re,math,random
import argparse
import csv

# advisor-assign.py
#  Tyler Bletsch (Tyler.Bletsch@duke.edu)
#
# Assign advisors to students based on two CSV files and several rules
#


def windows_color_compatibility():
    # Check if the platform is Windows
    if os.name == 'nt':
        os.system('')  # Enables the ANSI escape sequences in Windows
        #The os.system('') call effectively sends an empty command to the system shell. While this does not seem to do anything, it actually triggers the initialization of the console's handling of ANSI escape codes in certain versions of Windows 10.
        # note: microsoft should be embarassed by the above
    

# print only in verbose mode
def vprint(*v):
    # ANSI escape codes for colors
    CYAN = '\033[36m'
    RESET = '\033[0m'
    global verbose
    if verbose: 
        sys.stdout.write(CYAN)
        print(*v)
        sys.stdout.write(RESET)
    
# strip surrounding whitespace and make any inner whitespace just one space
def normalize_whitespace(s):
    s = s.strip() # remove surrounding whitespace
    s = re.sub(r'\s+', ' ', s) # normalize whitespace
    return s
    
# cleans up a person's name, and converts to "FIRST LAST" from "LAST, FIRST" if needed
def normalize_name(name):
    name = normalize_whitespace(name)
    
    name = re.sub(r'(.*), (.*)', r'\2 \1', name) # convert "LAST, FIRST" to "FIRST LAST" if needed
    # note: names with commas naturally in them will get incorrectly flipped, e.g. "Tyler Bletsch, PhD" -> "PhD Tyler Bletsch", so don't feed me those
    
    return name

# convert a name normalized to "FIRST LAST" to "LAST, FIRST"
def firstlast_to_lastfirst(name):
    return re.sub(r'(.*?) (.*)', r'\2, \1', name) # convert "LAST, FIRST" to "FIRST LAST" if needed

# ternary
def iff(c,a,b):
    if c: return a
    else: return b

def warn(msg):
    # ANSI escape codes for colors
    GOLD = '\033[33m'
    RESET = '\033[0m'
    
    print(GOLD + msg + RESET, file=sys.stderr)

# abort
def die(msg):
    # ANSI escape codes for colors
    RED = '\033[91m'
    RESET = '\033[0m'
    
    print(RED + msg + RESET, file=sys.stderr)
    sys.exit(1)

# find an element in a list that matches a regex
# used to find column names without a precise match
def find_first_matching_element(regex, elements, ignorecase = True):
    pattern = re.compile(regex, iff(ignorecase, re.IGNORECASE, None))

    for element in elements:
        if re.search(pattern, element):
            return element

    return None

# find element indices in a list that matches a regex
# used to find column numbers without a precise match
def find_all_matching_indices(regex, elements, ignorecase = True):
    pattern = re.compile(regex, iff(ignorecase, re.IGNORECASE, None))
    
    r = []
    for i,element in enumerate(elements):
        if re.search(pattern, element):
            r.append(i)

    return r

# find element index in a list that matches a regex
# used to find column numbers without a precise match
def find_first_matching_index(regex, elements, ignorecase = True):
    pattern = re.compile(regex, iff(ignorecase, re.IGNORECASE, None))
    
    for i,element in enumerate(elements):
        if re.search(pattern, element):
            return i

    return None

# read the faculty csv and build the faculty dictionary
# result dict:
#  { 
#    FACULTY_NAME: {'share': SHARE_VALUE, 'tracks': set(CHOSEN_TRACKS)},
#    ...
#  }
def get_faculty(faculty_csv):
    global verbose
    # column names
    COL_NAME = "Faculty Member"
    COL_SHARE = "Share"
    # note: we assume all columns right of SHARE are the tracks
    
    faculty = {}
    
    print(f"Loading faculty from '{faculty_csv}'...")
    
    with open(faculty_csv, mode='r', encoding='utf-8-sig') as file:
        reader = csv.DictReader(file)
        
        share_index = reader.fieldnames.index(COL_SHARE)
        track_names = reader.fieldnames[share_index + 1:]
        
        vprint(f"Tracks: {track_names}")
        for row in reader:
            name = row[COL_NAME]
            if row[COL_SHARE]:
                share = float(row[COL_SHARE])
            else:
                share = 0 # assume blank share means zero
            tracks = set(track_name for track_name in track_names if row[track_name])
            
            name = name.strip() # remove surrounding whitespace
            new_name = normalize_name(name)
            if name != new_name:
                vprint(f"{faculty_csv}: Normalized '{name}' to '{new_name}'")
            name = new_name
            
            if name in faculty:
                warn(f"{faculty_csv}: Warning: Faculty '{name}' appears twice. Ignoring subsequent mentions.")
                continue
                
            if len(tracks)==0:
                warn(f"{faculty_csv}: Warning: Faculty '{name}' has no tracks and will only receive preference-selecting advisees. Is this right?")
                
            faculty[name] = {'share': share, 'tracks': tracks}
        
        print(f"Loaded {len(faculty)} faculty.")
        #vprint(faculty)
        return faculty

# read the student csv from qualtrics and build the student dictionary
#
# the qualtrics form involved should have questions for: last name, first name, track, and a rank-order question on advisor preferences
# the track names must exactly match the track column headers in the faculty csv
#
# student names and preferred faculty names are all normalized to "FIRST LAST"
# students with no preference have an empty preference list
#
# result dict:
#  { 
#    STUDENT_NAME: {'email': EMAIL, 'track': TRACK_NAME, 'program': PROGRAM_NAME, 'preferences': [FACULTY_NAME_1,FACULTY_NAME_2,FACULTY_NAME_3,FACULTY_NAME_4,FACULTY_NAME_5]},
#    ...
#  }
def get_students(student_csv):
    global verbose
    
    # okay, the qualtrics csv is a bit weird:
    # - the actual first row is question numbers, which may shift year to year, so we skip that line before parsing the csv at all
    # - the questions themselves follow, but for rank questions (which we use for advisor preference), the question is REPEATED for every option, with the actual choice appended:
    #     example: "If you have an advisor preference... - Ranks - Please select your top five choices. - Lawrence Carin - Rank"
    # - then there's a row of json trash we skip
    # - then we have actual student responses
    #
    # also, just to add pain, the same question is repeated per track, so we cant used a csv.DictReader, but rather column numbers
    # 
    # we use regex to find the column numbers by fuzzy match of name, since the form may be phrased subtly different in the future
    # we also use regex to extract the faculty name for preferences from the end of the very long question field names
    
    
    # column names for this input will by fuzzy matched by regex
    REGEX_COL_LAST_NAME = r"last.*name"
    REGEX_COL_FIRST_NAME = r"first.*name"
    REGEX_COL_EMAIL = r"e-?mail"
    REGEX_COL_TRACK = r"(track|area of interest)"
    REGEX_COL_PROGRAM = r"master'?s.*program"
    REGEX_COL_PREFERENCE = r"advisor preference.*? - ([\w\s,]*?)( - Rank)?$" # this regex also extracts the faculty name into regex group 1
    
    students = {}
    
    print(f"Loading students from '{student_csv}'...")
    
    with open(student_csv, mode='r', encoding='utf-8-sig') as file:
        reader = csv.reader(file)
        
        next(reader) # eat the first line of "Q7	Q8	Q22	Q2	Q4_0_1_RANK ..."

        fieldnames = next(reader) # get field names: the question text row gets used as these
        
        #next(reader) # eat the row of json trash after the question text row
        # ^ note: recent inputs no longer have this input, so removing this skip.
        # if you have json trash, just remove it from input before running this
        
        # let's figure out these column numbers by regex
        COL_LAST_NAME = find_first_matching_index(REGEX_COL_LAST_NAME,fieldnames)
        if COL_LAST_NAME is None: die(f"{student_csv}: Error: Unable to find a last name column")

        COL_FIRST_NAME = find_first_matching_index(REGEX_COL_FIRST_NAME,fieldnames)
        if COL_FIRST_NAME is None: die(f"{student_csv}: Error: Unable to find a first name column")

        COL_EMAIL = find_first_matching_index(REGEX_COL_EMAIL,fieldnames)
        if COL_EMAIL is None: die(f"{student_csv}: Error: Unable to find an email column")

        COL_TRACK = find_first_matching_index(REGEX_COL_TRACK,fieldnames)
        if COL_TRACK is None: die(f"{student_csv}: Error: Unable to find a track column")

        COL_PROGRAM = find_first_matching_index(REGEX_COL_PROGRAM,fieldnames)
        if COL_PROGRAM is None: die(f"{student_csv}: Error: Unable to find a program column")

        COLS_PREFERENCE = find_all_matching_indices(REGEX_COL_PREFERENCE,fieldnames) # unlike the above, this is a LIST of col indexes
        if not COLS_PREFERENCE: die(f"{student_csv}: Error: Unable to find at least one faculty advisor preference column")
        
        REGEX_NO_PREFERENCE = "No preference" # the string to indicate no preference
        
        vprint("Parsing preference columns for faculty names...")
        col_num_to_pref = {}
        for col_num in COLS_PREFERENCE:
            col_name = fieldnames[col_num]
            #col_name = re.sub(r' - Rank$','',col_name)
            m = re.search(REGEX_COL_PREFERENCE, col_name, re.IGNORECASE)
            #print(col_name)
            #print(m.groups())
            pref_advisor = m.group(1)
            if not pref_advisor:
                die(f"{student_csv}: Unable to parse faculty preference from column {col_num} ('{col_name}'")
            pref_advisor = normalize_name(pref_advisor)
            col_num_to_pref[col_num] = pref_advisor
            vprint(f"  [{col_num}] Found '{pref_advisor}'")

        vprint(f"""Identified the following columns in {student_csv}: 
    Last name: '{fieldnames[COL_LAST_NAME]}' (column {COL_LAST_NAME})
    First name: '{fieldnames[COL_FIRST_NAME]}' (column {COL_FIRST_NAME})
    Email: '{fieldnames[COL_EMAIL]}' (column {COL_EMAIL})
    Track: '{fieldnames[COL_TRACK]}' (column {COL_TRACK})
    Program: '{fieldnames[COL_PROGRAM]}' (column {COL_PROGRAM})
    Preferences: '{fieldnames[COLS_PREFERENCE[0]]}' and similar (columns {COLS_PREFERENCE})
""")
        
        # okay, actually process students now
        for row in reader:
            # normalize name
            first_name = row[COL_FIRST_NAME]
            first_name = normalize_whitespace(first_name)
            last_name = row[COL_LAST_NAME]
            last_name = normalize_whitespace(last_name)
            if not first_name and not last_name: continue # assume this is a blank row
            if not first_name or not last_name:  # only ONE name is missing? that's bad
                warn(f"{student_csv}: Warning: Incomplete name (first='{first_name}' last='{last_name}')")
            name = f"{first_name} {last_name}"
            
            email = row[COL_EMAIL].strip()
            
            track = row[COL_TRACK]
            track = normalize_whitespace(track)
            
            program = row[COL_PROGRAM]
            program = normalize_whitespace(program)
            
            # look for each rank 1..5 and turn them into a list of faculty names
            preferences = []
            for rank in range(1,5+1):
                for col_num in COLS_PREFERENCE:
                    if row[col_num] == str(rank): # found the given rank
                        preference = col_num_to_pref[col_num]
                        
                        if re.search(REGEX_NO_PREFERENCE,preference,re.IGNORECASE):
                            # is this preference actually "no preference"? skip it then
                            continue
                        preferences.append(preference)
            
            record = {'email': email, 'track': track, 'program': program, 'preferences': preferences}
            
            if name not in students:
                students[name] = record
            else:
                warn(f"Duplicate student detected with name '{name}' -- ignoring duplicate:\n  Original: {students[name]}\n  Ignored:  {record}")
            
            #vprint(f"{name}: {students[name]}")
            
        print(f"Loaded {len(students)} students.")
        return students
        
def shuffled(input_list):
    shuffled_list = list(input_list).copy()
    random.shuffle(shuffled_list)
    return shuffled_list

# take in dictionaries of faculty and students as produced by get_faculty and get_students and do advisor assignments, thus updating these dictionaries
#
# input faculty dict:
#  { 
#    FACULTY_NAME: {'share': SHARE_VALUE, 'tracks': set(CHOSEN_TRACKS)},
#    ...
#  }
# input student dict:
#  { 
#    STUDENT_NAME: {'track': TRACK_NAME, 'program': PROGRAM_NAME, 'preferences': [FACULTY_NAME_1,FACULTY_NAME_2,FACULTY_NAME_3,FACULTY_NAME_4,FACULTY_NAME_5]},
#    ...
#  }
#
# if successful, the dictionaries will look like the following when done:
#
# modified faculty dict:
#  { 
#    FACULTY_NAME: {'share': SHARE_VALUE, 'tracks': set(CHOSEN_TRACKS), 'advisees': [ADVISEE_NAME_1, ...], 'popularity': SCORE},
#    ...
#  }
# modified student dict:
#  { 
#    STUDENT_NAME: {'track': TRACK_NAME, 'preferences': [FACULTY_NAME_1,FACULTY_NAME_2,FACULTY_NAME_3,FACULTY_NAME_4,FACULTY_NAME_5], 'advisor': FACULTY_NAME, 'how_assigned': REASON},
#    ...
#  }
# Note: 'how_assigned' will be one of "Rank N pick" (assigned a preferred advisor), "Random" (assigned a random advisor), or "Overflow" (a fall-back algorithm assigned the advisor potentially in excess of faculty slots).
def assign_advisors(faculty, students):
    # check tracks in faculty and student records to ensure correspondence
    f_tracks = set()
    for f in faculty.values():
        f_tracks.update(f['tracks'])
    s_tracks = set()
    for s in students.values():
        s_tracks.add(s['track'])
        
    if f_tracks != s_tracks:
        msg = ''
        if (f_tracks - s_tracks):
            msg += f"Error: the following tracks were in the faculty data but never present in the student data. Ensure track names match perfectly!\n  {f_tracks-s_tracks}\n"
        if (s_tracks - f_tracks):
            msg += f"Error: the following tracks were in the student data but never present in the faculty data. Ensure track names match perfectly!\n  {s_tracks-f_tracks}\n"
        die(msg)
    tracks = f_tracks
    vprint(f"Tracks: {tracks}")

    #compute number of advisees per faculty: share/sum(all shares) * num_students
    total_share = sum(f['share'] for f in faculty.values())
    num_students = len(students)
    vprint("Computing advisees per faculty...")
    for name in faculty:
        f = faculty[name] # abbrev
        share = f['share']
        f['advisees'] = []
        f['popularity'] = 0 # a silly metric to measure how much faculty are ranked by students in preferences
        
        # compute per-track slots
        vprint(f"  Slots for faculty '{name}' (share {share})...")
        slots = 0
        if not f['tracks']:
            warn(f"Warning: Faculty '{name}' has no listed tracks! They will get 0 advisee slots. Is this right?")
        for track in f['tracks']:
            num_students = sum(1 for s in students.values() if s['track']==track)
            track_total_share = sum(f['share'] for f in faculty.values() if track in f['tracks'])
            track_slots = f['share'] / track_total_share * num_students
            slots += track_slots
            vprint(f"    Track '{track}' has {num_students} students, faculty total share {track_total_share}, proportional share {f['share']/track_total_share:.03f}, so add {track_slots:.03f} slots.")
        f['slots'] = math.ceil(slots)
        vprint(f"  Total slots: {f['slots']}")
        #vprint(f"{name}: {f}")
        
    print("Assigning students with preferences...")
    
    #for each student in shuffle(students who give preferences):
    num_assigned_by_rank = 0
    for name in shuffled(students):
        s = students[name] # abbrev
        s['advisor'] = None
        if not s['preferences']: continue # skip students with no preference, we handle them later
        vprint(f"  Student: '{name}'")
        
        #for each preference:
        #    assign advisor at that preference rank, unless advisee is "full"
        #    if still no advisor because all preferences are full, leave unset (will be handled with the no-pref students)
        for rank0,f_name in enumerate(s['preferences']):
            rank = rank0+1 # convert 0 origin to 1 origin
            if f_name not in faculty: die(f"Error: Student '{name}' has a preference for faculty '{f_name}', but they're not in the given faculty -- ensure names match exactly")
            f = faculty[f_name] # abbrev for this preferred faculty's record
            f['popularity'] += (5-rank)+1 # rank 1 gives 5 points, 2 gives 4 points, etc.
            if len(f['advisees']) < f['slots']: # enough room?
                # assign!
                f['advisees'].append(name)
                s['advisor'] = f_name
                s['how_assigned'] = f"Rank {rank} pick"
                num_assigned_by_rank += 1
                vprint(f"    ASSIGN: '{f_name}' (their #{rank} pick, advisor has {len(f['advisees'])}/{f['slots']} advisees now)")
                break
            else:
                vprint(f"    Full:   '{f_name}' (their #{rank} pick)? No - they're full")
        if not s['advisor']: 
            vprint(f"    Defer:  Didn't get any of their preferred advisors, will assign randomly later")
            
    print(f"Assigned {num_assigned_by_rank} students to one of their ranked preferences.")
    
    print("Assigning remaining students...")
    
    #for each student in shuffle(students with no advisor set yet):
    #    # note: this is no-preference students as well as students whose perferences were all full
    #    assign random advisor within their track
    num_assigned_random = 0
    num_assigned_overflow = 0
    for name in shuffled(students):
        s = students[name] # abbrev
        if s['advisor']: continue # skip students who already have an advisor now
        vprint(f"  Student: '{name}'")
        
        track = s['track']
        track_faculty_names = shuffled(f_name for f_name in faculty if track in faculty[f_name]['tracks']) # get faculty in the student's chosen track, shuffled
        if not track_faculty_names: die(f"Error: Student '{name}' has track '{track}' for which there are NO faculty listed! Make sure track names match exactly between faculty and student files.")
        
        for f_name in track_faculty_names:
            f = faculty[f_name] # abbrev for this faculty's record
            if len(f['advisees']) < f['slots']: # enough room?
                # assign!
                f['advisees'].append(name)
                s['advisor'] = f_name
                s['how_assigned'] = f"Random"
                num_assigned_random += 1
                vprint(f"    ASSIGN: '{f_name}' (randomly picked from track '{track}', advisor has {len(f['advisees'])}/{f['slots']} advisees now)")
                break
            else:
                vprint(f"    Full:   '{f_name}' (random track advisor)? No, they're full")
        if not s['advisor']: 
            vprint(f"    OVERFLOW: Didn't get *any* advisor, so we'll assign them to the track faculty with the lowest advisees per share")
            
            def get_aps(f_name):
                # advisees per share, a metric of equitable advising load
                return len(faculty[f_name]['advisees'])/faculty[f_name]['share']
            
            f_name_min = min(track_faculty_names, key=get_aps)
            if verbose:
                # this re-computation is purely for verbose-mode debug printing
                vprint("    Computing advisees-per-share:")
                for f_name in track_faculty_names:
                    vprint(f"      Faculty '{f_name}': {len(faculty[f_name]['advisees'])} advisees, share {faculty[f_name]['share']} => {get_aps(f_name)}")
            f = faculty[f_name_min]
            num_assigned_overflow += 1
            f['advisees'].append(name)
            s['advisor'] = f_name_min
            s['how_assigned'] = f"Overflow"
            vprint(f"    ASSIGN: '{f_name_min}' (advisor has {len(f['advisees'])}/{f['slots']} advisees now)")
            warn(f"Warning: Student '{name}' didn't get any advisor by normal means, so they were assigned advisor '{f_name_min}' in overflow mode.")
    
    print(f"Assigned {num_assigned_random} students randomly, had to do overflow assignment for {num_assigned_overflow} students.")
    
    print("Advisor assignments complete.")
            
def write_faculty_output(faculty, out_faculty_csv):
    # Write to output CSV
    print(f"Writing faculty info to '{out_faculty_csv}'...")
    with open(out_faculty_csv, mode='w', newline='') as file:
        faculty_writer = csv.writer(file)
        
        faculty_writer.writerow(['Faculty','Tracks','Share','Popularity','Slots','Slots used','Advisees'])
        for f_name in faculty:
            f = faculty[f_name]
            faculty_writer.writerow([f_name,", ".join(f['tracks']),f['share'],f['popularity'],f['slots'],len(f['advisees']),", ".join(f['advisees'])])
        
def write_student_output(students, out_student_csv):
    # Write to output CSV
    print(f"Writing student info to '{out_student_csv}'...")
    with open(out_student_csv, mode='w', newline='') as file:
        student_writer = csv.writer(file)
        
        student_writer.writerow(['Name (F L)','Name (L,F)','Email', 'Track','Program','Preferences','Advisor','How assigned'])
        for name in students:
            lastfirst_name = firstlast_to_lastfirst(name)
            s = students[name]
            student_writer.writerow([name,lastfirst_name,s['email'],s['track'],s['program'],", ".join(s['preferences']),s['advisor'],s['how_assigned']])
        

def main():
    windows_color_compatibility()

    parser = argparse.ArgumentParser(description="Process faculty and student CSV files.")
    
    parser.add_argument('input_faculty_csv', type=str, help='Path to the input faculty CSV file.')
    parser.add_argument('input_student_csv', type=str, help='Path to the input student CSV file.')
    parser.add_argument('output_faculty_csv', type=str, help='Path to the output faculty CSV file.')
    parser.add_argument('output_student_csv', type=str, help='Path to the output student CSV file.')
    parser.add_argument('-v', '--verbose', action='store_true', help='Increase output verbosity.')
    parser.add_argument('-r', '--true-random', action='store_true', help='Make *different* random choices every time instead of repeatable random choices (i.e., non-constant seed).')
    
    args = parser.parse_args()
    
    if args.output_faculty_csv == args.input_faculty_csv or args.output_faculty_csv == args.input_student_csv:
        die(f"Error: the given output faculty csv {args.output_faculty_csv} is the same as an input file, aborting.")
    if args.output_student_csv == args.input_faculty_csv or args.output_student_csv == args.input_student_csv:
        die(f"Error: the given output student csv {args.output_student_csv} is the same as an input file, aborting.")
    
    if not args.true_random:
        random.seed(2024) # put whatever number you like here to change the default repeatable random choices

    global verbose
    verbose = args.verbose

    faculty = get_faculty(args.input_faculty_csv)
    
    students = get_students(args.input_student_csv)
    
    assign_advisors(faculty, students) # updates provided dictionaries with advisor info
    
    write_faculty_output(faculty, args.output_faculty_csv)
    
    write_student_output(students, args.output_student_csv)
    
    print("All done!")

if __name__ == "__main__":
    main()
